"use client";
import { PersonFill } from "react-bootstrap-icons";
import { signIn} from "next-auth/react";

import BotaoLogin from "../components/BotaoLogin";

export default function Login() {
  return (
    <>
        <section className="secao">
          <div className="container">
            <div className="row">
              <div className="col d-flex align-items-center">
                <div className="display-1 fw-bold">FC &#60; &#8725; &#62;</div>
              </div>
              <div className="col d-flex justify-content-center">
                <div className="row">
                  <div className="col-12 d-flex justify-content-center">
                    <div className="fs-1">
                      <PersonFill />
                    </div>
                  </div>
                  <div className="col-12 d-flex justify-content-center">
                    <div className="fs-1 fw-bold">Login</div>
                  </div>
                  <div className="col-12 d-flex justify-content-center">
                    <BotaoLogin />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </>
  );
}
