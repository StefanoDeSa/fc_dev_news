"use client";
import { signOut, useSession } from "next-auth/react";
import { create } from "../services/userService";
import { useEffect } from "react";
import Link from "next/link";

export default function Admin() {
  const { data: session } = useSession();

  useEffect(() => {
    if (session?.user) {
      const { user } = session;

      create({ usuario: user })
        .then(() => {
          console.log("Usuário criado com sucesso!");
        })
        .catch((error) => {
          console.error("Erro ao criar usuário:", error);
        });
    }
  }, [session]);

  return (
    <div className="container">
      <h1 className="display-6 my-3">Tela de usuário</h1>
      <div className="row my-3 align-items-center">
        <div className="col">
          <img
            className="rounded-circle"
            src={session?.user.image}
            style={{ width: 60 }}
          />
        </div>
        <div className="col">
          <div className="row">
            <div className="col-12 d-flex justify-content-end">
              <p>Nome: {session?.user.name}</p>
            </div>
            <div className="col-12 d-flex justify-content-end">
              <p>Email: {session?.user.email}</p>
            </div>
          </div>
        </div>
      </div>
      <div className="row my-3">
        <div className="my-2">
          <Link href="admin/gerenciador">
            <button type="button" className="btn btn-primary">
              Gerenciador de notícias
            </button>
          </Link>
        </div>
        <div className="my-2">
          <button type="button" className="btn btn-danger" onClick={() => signOut({ callbackUrl: '/login/' })}>Sair da conta</button>
        </div>
      </div>
    </div>
  );
}
