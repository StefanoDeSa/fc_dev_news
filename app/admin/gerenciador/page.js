'use client'
import CardNoticiaAdmin from "@/app/components/CardNoticiaAdmin"
import Link from "next/link"
import { Plus } from "react-bootstrap-icons"
import { Form, Button} from "react-bootstrap"
import { findAllNoticias } from "@/app/services/NoticiaService"
import { useEffect, useState } from "react"

export default function Gerenciador(){
    const [noticias, setNoticias] = useState([]);


    useEffect(() => {
        const fetchNoticias = async () => {
          try {
            const noticiasData = await findAllNoticias();
            setNoticias(noticiasData);
          } catch (error) {
            console.error("Erro ao obter as noticias:", error);
          }
        };
    
        fetchNoticias();
      }, []);

        return(
            <div className="container">
                <div className="row my-3 align-items-center">
                    <div className="col">
                        <h1 className="display-6">Gerenciador de Noticias</h1>
                    </div>
                    <div className="col">
                        <Form className="d-flex">
                            <Form.Control
                            type="search"
                            placeholder="Search"
                            className="me-2"
                            aria-label="Search"
                            />
                        <Button variant="outline-success">Search</Button>
                        </Form>
                    </div>
                    <div className="col-2 justify-content-end">
                   <Link href='gerenciador/cadastrar'><button type="button" className="btn btn-primary rounded-circle"><Plus style={{fontSize:20}}/></button></Link> 
                    </div>
                </div>
                {noticias.map((noticia) => (
                   <Link className="text-decoration-none noticia-link" key={noticia.id} href={`gerenciador/editar/${noticia.id}/`}><CardNoticiaAdmin noticia={noticia} /></Link>
                ))}
    
            </div>
        )
    }