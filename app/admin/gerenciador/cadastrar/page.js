'use client'
import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { findAllTags } from "@/app/services/TagService";
import { createNoticia } from "@/app/services/NoticiaService";
import { useSession } from "next-auth/react";

export default function CadastroNoticia() {
  const { data: session } = useSession();

  const [titulo, setTitulo] = useState("");
  const [corpoNoticia, setCorpoNoticia] = useState("");
  const [imagemNoticia, setImagemNoticia] = useState("");
  const [tagNoticia, setTagNoticia] = useState("");
  const [tags, setTags] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case "titulo":
        setTitulo(value);
        break;
      case "corpoNoticia":
        setCorpoNoticia(value);
        break;
      case "imagemNoticia":
        setImagemNoticia(value);
        break;
      case "tagNoticia":
        setTagNoticia(value);
        break;
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    setIsLoading(true);

    const dadosPublicar = {
      titulo: titulo,
      corpo: corpoNoticia,
      autor: session.user.email,
      tag: tagNoticia,
      status: false,
      imagem: imagemNoticia
    };

    try {
      await createNoticia({ dados: dadosPublicar });
      console.log("Notícia criada com sucesso!");
      setTitulo("");
      setCorpoNoticia("");
      setImagemNoticia("");
      setTagNoticia("");
    } catch (error) {
      console.error("Erro ao criar notícia:", error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    const fetchTags = async () => {
      try {
        const tagsData = await findAllTags();
        setTags(tagsData);
      } catch (error) {
        console.error("Erro ao obter as tags:", error);
      }
    };

    fetchTags();
  }, []);

  return (
    <div className="container my-3">
      <Form onSubmit={handleSubmit}>
        <div className="row">
          <div className="col-6">
            <Form.Group className="mb-3" controlId="titulo">
              <Form.Label>Título da Notícia</Form.Label>
              <Form.Control
                type="text"
                name="titulo"
                value={titulo}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="corpoNoticia">
              <Form.Label>Corpo da Notícia</Form.Label>
              <Form.Control
                as="textarea"
                rows={10}
                name="corpoNoticia"
                value={corpoNoticia}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="imagemNoticia">
              <Form.Label>Imagem da Notícia</Form.Label>
              <Form.Control
                type="text"
                name="imagemNoticia"
                value={imagemNoticia}
                onChange={handleInputChange}
              />
            </Form.Group>
          </div>
          <div className="col-6">
            <Form.Label>Tag da Notícia</Form.Label>
            <Form.Select
              aria-label="Default select example"
              name="tagNoticia"
              value={tagNoticia}
              onChange={handleInputChange}
            >
              <option value="--"> -- </option>
              {tags.map((tag) => (
                <option key={tag.slug} value={tag.slug}>
                  {tag.nome}
                </option>
              ))}
            </Form.Select>
          </div>
        </div>
        <div className="row my-3">
          <div className="col-1 d-flex">
            <Button variant="primary" name="cadastrar" type="submit" disabled={isLoading}>
              {isLoading ? "Aguarde..." : "Cadastrar"}
            </Button>
          </div>
        </div>
      </Form>
    </div>
  );
}
