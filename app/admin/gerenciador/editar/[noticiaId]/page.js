'use client'
import { findById, updateNoticia, publishNoticia } from "@/app/services/NoticiaService"
import { findAllTags } from "@/app/services/TagService"
import { useEffect, useState} from "react"
import { useSession } from "next-auth/react"
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

export default function Editar({params}){
    const [noticia, setNoticia] = useState({})
    const {noticiaId} = params
    
    const { data: session } = useSession();

    const [titulo, setTitulo] = useState("");
    const [corpoNoticia, setCorpoNoticia] = useState("");
    const [imagemNoticia, setImagemNoticia] = useState("");
    const [tagNoticia, setTagNoticia] = useState("");
    const [tags, setTags] = useState([]);
    const [buttonClicked, setButtonClicked] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        switch (name) {
          case "titulo":
            setTitulo(value);
            break;
          case "corpoNoticia":
            setCorpoNoticia(value);
            break;
          case "imagemNoticia":
            setImagemNoticia(value);
            break;
          case "tagNoticia":
            setTagNoticia(value);
            break;
        }
      };

    useEffect(() => {
        const fetchNoticia = async () => {
            try {
                const noticiaData = await findById(parseInt(noticiaId))
                setNoticia(noticiaData)
                setTitulo(noticiaData.titulo);
                setCorpoNoticia(noticiaData.corpo);
                setImagemNoticia(noticiaData.imagemUrl);

            } catch (error) {
                console.error("Erro ao obter a noticia:", error);
            }
        }

        const fetchTags = async () => {
            try {
              const tagsData = await findAllTags();
              setTags(tagsData);
            } catch (error) {
              console.error("Erro ao obter as tags:", error);
            }
          };


        fetchNoticia()
        fetchTags()
    }, [])

    const handleButtonClick = (button) => {
        setButtonClicked(button);
      };
    
    const handleSubmit = async (event) => {
        event.preventDefault();

        setIsLoading(true);

        switch(buttonClicked){
            case "editar":
                noticia.titulo = titulo
                noticia.corpo = corpoNoticia
                noticia.autorEmail = session?.user.email
                noticia.imagemUrl = imagemNoticia

                try {
                    await updateNoticia({ dados: noticia });
                    console.log("Notícia editada com sucesso!");

                } catch (error) {
                    console.error("Erro ao editar a notícia:", error);
                } finally {
                    setIsLoading(false);
                }

                break

            case "publicar":
                try {
                    await publishNoticia(parseInt(noticiaId));
                    console.log("Notícia publicado com sucesso!");

                } catch (error) {
                    console.error("Erro ao publicar a notícia:", error);
                } finally {
                    setIsLoading(false);
                }

                break
        }
        };


    return (
        <div className="container my-3">
          <Form onSubmit={handleSubmit}>
            <div className="row">
              <div className="col-6">
                <Form.Group className="mb-3" controlId="titulo">
                  <Form.Label>Título da Notícia</Form.Label>
                  <Form.Control
                    type="text"
                    name="titulo"
                    value={titulo}
                    onChange={handleInputChange}
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="corpoNoticia">
                  <Form.Label>Corpo da Notícia</Form.Label>
                  <Form.Control
                    as="textarea"
                    rows={10}
                    name="corpoNoticia"
                    value={corpoNoticia}
                    onChange={handleInputChange}
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="imagemNoticia">
                  <Form.Label>Imagem da Notícia</Form.Label>
                  <Form.Control
                    type="text"
                    name="imagemNoticia"
                    value={imagemNoticia}
                    onChange={handleInputChange}
                  />
                </Form.Group>
              </div>
              <div className="col-6">
                <Form.Label>Tag da Notícia</Form.Label>
                <Form.Select
                  aria-label="Default select example"
                  name="tagNoticia"
                  value={tagNoticia}
                  onChange={handleInputChange}
                >
                  <option value="--"> -- </option>
                  {tags.map((tag) => (
                    <option key={tag.slug} value={tag.slug}>
                      {tag.nome}
                    </option>
                  ))}
                </Form.Select>
              </div>
            </div>
            <div className="row my-3">
              <div className="col-1 d-flex">
                <Button variant="primary" name="Editar" type="submit" disabled={isLoading} onClick={() => handleButtonClick("editar")}>
                  {isLoading ? "Aguarde..." : "Editar"}
                </Button>
              </div>
              <div className="col-1 d-flex">
                <Button variant="success" name="Publicar" type="submit" disabled={isLoading} onClick={() => handleButtonClick("publicar")}>
                  {isLoading ? "Aguarde..." : "Publicar"}
                </Button>
              </div>
            </div>
          </Form>
        </div>
      );
}