'use client'
import { useSession } from "next-auth/react"
import NaoAutenticado from "../components/NaoAutenticado";

export default function AdminLayout({ children }) {
  const { data: session } = useSession();

  if (!session) {
    return <NaoAutenticado />;
  } else {
    return children;
  }
}
