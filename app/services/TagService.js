'use server'
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()


export async function findAllTags(){
    const tags = await prisma.tag.findMany()
    return tags
}

export async function findTagBySlug(slug){

    if(slug){
        const tag = await prisma.tag.findUnique({
            where: {
                slug: slug
            }
        })
        return tag
    }
    
}