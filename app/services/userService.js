'use server'
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export async function create({usuario}){
    const userdb =  await findUsuario(usuario.email)
    if(!userdb){
        const user = await prisma.user.create({
            data: {
              urlImagem: `${usuario.image}`,
              email: `${usuario.email}`,
              nome: `${usuario.name}`,
            },
          })
          console.log(user)
        }
    }

export async function findUsuario(email){

    const user = await prisma.user.findFirst({
        where: {
          email: email,
        },
      })

    return user
      
}