'use server'
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export async function createNoticia({dados}){
    const noticia = await prisma.noticia.create({
        data: {
            titulo: dados.titulo,
            corpo: dados.corpo,
            autorEmail: dados.autor,
            tagSlug: dados.tag,
            status: dados.status,
            imagemUrl: dados.imagem
        }
        }
    )   
    console.log(noticia)
}


export async function findAllNoticias(){
    const noticias = await prisma.noticia.findMany()
    return noticias
}

export async function findPublishedNoticias(){
    const noticias = await prisma.noticia.findMany({
        where: {
            status: true
        }
    })
    return noticias
}

export async function findById(noticiaId){
    const noticia = await prisma.noticia.findFirst({
        where: {
            id: noticiaId
        }
    })
    return noticia
}

export async function updateNoticia({dados}){
    const noticia = await prisma.noticia.update({
        where: {
          id: dados.id
        },
        data: {
            titulo: dados.titulo,
            corpo: dados.corpo,
            autorEmail: dados.autor,
            imagemUrl: dados.imagem
        }
      })
      console.log(noticia)
}

export async function publishNoticia(noticiaId){
    const noticia = await prisma.noticia.update({
        where: {
            id: noticiaId
        },
        data: {
            status: true
        }
    })
}

export async function unpublishNoticia(noticiaId){
    const noticia = await prisma.noticia.update({
        where: {
            id: noticiaId
        },
        data: {
            status: false
        }
    })
}