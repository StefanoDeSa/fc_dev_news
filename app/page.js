import { Container } from "react-bootstrap";
import Link from "next/link";

export default function Home() {
  return (
    <div className="container my-2">
      <div className="row">
        <div class = "col">
          {/* 1 CARD - CARD COM A IMAGEM GRANDE */}
          <div className="card text-bg-dark">
            <img src="images/71.jpg" className="card-img" alt="img 71"></img>
            <div className="card-img-overlay">
              <h2 className="card-title fw-bold" style={{position: "absolute", bottom: "0", left: "0", right: "0", marginbottom: "20px"}}>Mulheres lideram entre números de formandos da Ulbra Palmas</h2>
              <a className="card-text stretched-link text-decoration-none" href="#"></a>
            </div>
          </div>
        </div>
        <div class = "col">
          <div className="card mb-3" style={{maxwidth: "540px"}}>
            <div className="row g-0">
              <div className="col-md-4">
                <img src="images/72.jpg" className="img-fluid rounded-start" alt="img 72"></img>
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <h5 className="card-title">Celebração dos 51 anos da ULBRA</h5>
                  <p className="card-text">Campus Palmas cantou os parabéns com colaboradores e alunos</p>
                  <a className="card-text stretched-link text-decoration-none" href="#"></a>
                  <p className="card-text"><small className="text-body-secondary border border-success fst-italic">Entretenimento</small></p>
                </div>
              </div>
            </div>
          </div>
          <div className="card mb-3" style={{maxwidth: "540px"}}>
            <div className="row g-0">
              <div className="col-md-4">
                <img src="images/73.jpg" className="img-fluid rounded-start" alt="img 73"></img>
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <h5 className="card-title">Clínica de Direitos Humanos inicia processo seletivo para o primeiro semestre de 2024</h5>
                  <p className="card-text">Os interessados tem até o dia 10 de março de 2024 para se inscreverem.</p>
                  <a className="card-text stretched-link text-decoration-none" href="#"></a>
                  <p className="card-text"><small className="text-body-secondary border border-success fst-italic">Saúde</small></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container my-2">
          <div class = "row">
            <div className="col-6">
              <div className="card mb-3" style={{maxwidth: "540px"}}>
                <div className="row g-0">
                  <div className="col-md-3">
                    <img src="images/74.jpg" className="img-fluid rounded-start" alt="img 73"></img>
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <h5 className="card-title">Oportunidade de Monitoria Voluntária</h5>
                      <p className="card-text">Inscrições Abertas para Disciplinas do 2024/1.</p>
                      <a className="card-text stretched-link text-decoration-none" href="#"></a>
                      <p className="card-text"><small className="text-body-secondary border border-success fst-italic">Educação</small></p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card mb-3" style={{maxwidth: "540px"}}>
                <div className="row g-0">
                  <div className="col-md-3">
                    <img src="images/75.jpg" className="img-fluid rounded-start" alt="img 73"></img>
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <h5 className="card-title">Mulheres na Ciência</h5>
                      <p className="card-text">Uma jovem pesquisadora em formação.</p>
                      <a className="card-text stretched-link text-decoration-none" href="#"></a>
                      <p className="card-text"><small className="text-body-secondary border border-success fst-italic">Educação</small></p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card mb-3" style={{maxwidth: "540px"}}>
                <div className="row g-0">
                  <div className="col-md-3">
                    <img src="images/76.jpg" className="img-fluid rounded-start" alt="img 73"></img>
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <h5 className="card-title">Semana Pedagógica 2024.1</h5>
                      <p className="card-text">O retorno dos docentes é marcado pela semana de atividade pedagógica.</p>
                      <a className="card-text stretched-link text-decoration-none" href="#"></a>
                      <p className="card-text"><small className="text-body-secondary border border-success fst-italic">Educação</small></p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="d-flex justify-content-center">
                {/* BOTÃO VER TODAS AS NOTICIAS */}
                <Link className="btn btn-primary" href="/noticias" role="button">Ver todas as noticias</Link>
              </div>
            </div>
              {/* COLUNA DAS PRINCIPAIS NOTICIAS VIU ISSO AQUI  */}
              <div className="col-4 border mx-auto" >
                <br></br>
                <p className="h5 fw-bold d-flex justify-content-center">Viu isso aqui ?</p><hr></hr>
                {/* Card 1 - dentro do viu isso aqui  */}
                <div className="card mb-3" style={{maxwidth: "540px"}}>
                  <div className="row g-0">
                    <div className="col-md-4">
                      <img src="images/77.jpg" className="img-fluid rounded-start" alt="img 73"></img>
                    </div>
                    <div className="col-md-8">
                      <div className="card-body">
                        <h5 className="card-title">Hospital Veterinário da Ulbra Palmas atende veado</h5>
                        <a className="card-text stretched-link text-decoration-none" href="#"></a>
                        <p className="card-text"><small className="text-body-secondary border border-success fst-italic">Meio Ambiente</small></p>  
                      </div>    
                    </div>
                  </div>
                </div>
                {/* Card 2 - dentro do viu isso aqui  */}
                <div className="card mb-3" style={{maxwidth: "540px"}}>
                  <div className="row g-0">
                    <div className="col-md-4">
                      <img src="images/78.jpg" className="img-fluid rounded-start" alt="img 73"></img>
                    </div>
                    <div className="col-md-8">
                      <div className="card-body">
                        <h5 className="card-title">Inovação e empreendedorismo</h5>
                        <a className="card-text stretched-link text-decoration-none" href="#"></a>
                        <p className="card-text"><small className="text-body-secondary border border-success fst-italic">Nacional</small></p>
                      </div>
                    </div>
                  </div>
                </div>
                {/* Card 3 - dentro do viu isso aqui  */}
                <div className="card mb-3" style={{maxwidth: "540px"}}>
                  <div className="row g-0">
                    <div className="col-md-4">
                      <img src="images/79.jpg" className="img-fluid rounded-start" alt="img 73"></img>
                    </div>
                    <div className="col-md-8">
                      <div className="card-body">
                        <h5 className="card-title">Visita Técnica ao Hospital do amor</h5>
                        <a className="card-text stretched-link text-decoration-none" href="#"></a>
                        <p className="card-text"><small className="text-body-secondary border border-success fst-italic">Saúde</small></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  );
}