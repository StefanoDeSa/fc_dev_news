export default function Comentar(){
    return (
        <div className="conteiner my-2">
            <div class ="row">
                <div class="col-md-1"> 
                    <div class="text-center">
                        <img src="images/71.jpg" class="rounded-circle" alt="foto" style={{width: "70px", height: "70px", marginTop: "11px"}}></img>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-floating">
                        <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style={{height: "100px"}}></textarea>
                        <label for="floatingTextarea2">Escreva um comentario...</label>
                    </div>
                </div>
                <div class= "col-md-1">
                    <input class="btn btn-primary" type="submit" value="Comentar" style={{marginTop: "30px"}}></input>
                </div>
            </div>
        </div>
    )
}