"use client";
import { signIn, signOut, useSession } from "next-auth/react";


export default function BotaoLogin(){
    const { data: session } = useSession()

    if (session && session.user) {
    return (
      <div>
        <button onClick={() => signOut()}>
          Sair
        </button>
      </div>
    );
  }
  return (
    <button onClick={() => signIn('google', { callbackUrl: '/admin' })}>
      Entrar
    </button>
  );
}