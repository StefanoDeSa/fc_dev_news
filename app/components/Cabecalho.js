"use client";

import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Link from "next/link";
import { useSession } from "next-auth/react";

function Cabecalho() {

  const { data: session } = useSession();
  
  return (
    <Navbar className="text-bg-primary p-3">
      <Container>
        <Navbar.Brand href="/">
          <span
            style={{
              display: "block",
              textAlign: "center",
              fontWeight: "bold",
              marginTop: "-15px",
              fontSize: "35px",
              color: "white",
            }}
          >
            FC
          </span>
          <span
            style={{
              display: "block",
              fontWeight: "bold",
              marginTop: "-15px",
              color: "white",
            }}
          >
            News
          </span>
        </Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            <Link href={session ? '/admin/' : '/login'}>
            <img
              src={session ? `${session.user.image}` : '/images/G_logo_branca.svg'}
              alt="User"
              style={{ width: "40px" }}
              className="rounded-circle"
            />
            </Link>
          </Navbar.Text>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Cabecalho;
