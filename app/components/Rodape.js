export default function Rodape() {
  return (
    <div id="rodape">
      <div className="container">
        <div className="row">
          <div style={{height:'75px'}} className="col d-flex justify-content-center align-items-center">
            <div >Portal de Noticias FC Dev News ❤</div>
          </div>
        </div>
      </div>
    </div>
  );
}
