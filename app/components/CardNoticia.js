import { imageConfigDefault } from "next/dist/shared/lib/image-config";

function CardNoticia({noticia}) {
  return (
    <div className="Container">
        <div className="my-5">
            <div className="card mb-3">
                <div className="d-flex g-0">
                    <div className="card-img rounded-start flex-shrink-0" style={{width: "250px", height: "180px"}}>
                        <img src={noticia.imagemUrl} alt="Imagem Noticia" style={{width:"100%", height:"100%", objectFit:"cover"}}/>
                    </div>
                    <div className="card-body flex-grow">
                        <h5 className="card-title fw-bold">
                            {noticia.titulo}
                        </h5>

                        <p className="card-text">
                            {noticia.corpo}
                        </p>
                    </div>
                </div>
            </div>
         </div>
    </div>

  );
}

export default CardNoticia;
