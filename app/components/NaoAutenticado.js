import Link from "next/link";

export default function NaoAutenticado(){
    return (
    <div>
    <p>Acesso não autorizado. Faça <Link href='/login'>login</Link> para continuar.</p>
    </div>
    )
}