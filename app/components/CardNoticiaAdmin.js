export default function CardNoticiaAdmin({noticia}) {
  return (
    <div className="card my-3 card-noticia">
      <div className="row g-0">
        <div className="col-md-2">
          <img
            src={noticia.imagemUrl}
            className="img-fluid rounded-start"
            alt="..."
          />
        </div>
        <div className="col-md-8">
          <div className="card-body">
            <h5 className="card-title">
              {noticia?.titulo}
            </h5>
            <p className="card-text">
              {noticia?.corpo}
            </p>
            <div className="row">
              <div className="col">
                <p className="card-text">
                  <small className="text-body-secondary">08/03/24</small>
                </p>
              </div>
              <div className="col d-flex justify-content-end">
                <p className="card-text">
                  Status: <small className="text-body-secondary">{noticia.status ? 'Publicado' : 'Salvo'}</small>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
