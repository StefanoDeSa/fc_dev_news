'use client'
import { useEffect, useState } from "react"
import { findById } from "@/app/services/NoticiaService"
import { findUsuario } from "@/app/services/userService"
import { findTagBySlug } from "@/app/services/TagService"

export default function Noticia({params}){
    const {noticiaId} = params
    const [noticia, setNoticia] = useState({})
    const [usuarioNoticia, setUsuarioNoticia] = useState({})
    const [tagNoticia, setTagNoticia] = useState({})

    useEffect(() => {
        const fetchNoticia = async () => {
            try {
                const noticiaData = await findById(parseInt(noticiaId))
                setNoticia(noticiaData)

                const tagData = await findTagBySlug(noticiaData.tagSlug)
                setTagNoticia(tagData)

                const userData = await findUsuario(noticiaData.autorEmail)
                setUsuarioNoticia(userData)


            } catch (error) {
                console.error("Erro ao obter a noticia:", error);
            }
        }

        fetchNoticia()
    }, [])

    return(
        <>
        <section className="secao">
            <div className="container justify-content-center">
                <h1 className="display-6 fw-bold">{noticia.titulo}</h1>
                <small>Por: {usuarioNoticia.nome}</small>
            </div>
        </section>
        <section className="secao">
        <div className="container my-3">
      <div class="card text-bg-dark">
      <img className="img-noticia" src={noticia.imagemUrl} />
      <div class="card-img-overlay">
        <div className="row">
            <div className="col-1 d-flex ms-3 justify-content-center">
            <button type="button" class="btn btn-success">{tagNoticia?.nome}</button>
            </div>
        </div>
      </div>
    </div>
    <div className="row my-3">
        <div className="col">
            <p className="fs-3">{noticia.corpo}</p>
        </div>
    </div>
    </div>
    </section>
        </>
        
    )
}