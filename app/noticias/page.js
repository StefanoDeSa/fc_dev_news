'use client'
import CardNoticia from "../components/CardNoticia";
import { findPublishedNoticias } from "../services/NoticiaService";
import { useState, useEffect } from "react";
import Link from "next/link";

export default function TodasNoticias() {

  const [noticias, setNoticias] = useState([]);


    useEffect(() => {
        const fetchNoticias = async () => {
          try {
            const noticiasData = await findPublishedNoticias();
            setNoticias(noticiasData);
          } catch (error) {
            console.error("Erro ao obter as noticias:", error);
          }
        };
    
        fetchNoticias();
      }, []);

  return (
  <div className="container my-3 page-header-inner">
    <h1 className= "page-header-titulo"> Todas as notícias </h1>
    <div className= "page-header-subtitulo">Notícias publicadas no Portal FC Dev News </div>

    {noticias.map((noticia) => (
    <Link className="text-decoration-none noticia-link" key={noticia.id} href={`noticias/${noticia.id}`}><CardNoticia noticia={noticia} /></Link>
     ))}

  </div>
  )
}

