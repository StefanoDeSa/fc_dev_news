-- CreateTable
CREATE TABLE "Noticia" (
    "id" SERIAL NOT NULL,
    "dataPublicacao" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "titulo" TEXT NOT NULL,
    "corpo" TEXT NOT NULL,
    "autorEmail" TEXT NOT NULL,
    "tagSlug" TEXT NOT NULL,
    "status" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "Noticia_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Noticia" ADD CONSTRAINT "Noticia_autorEmail_fkey" FOREIGN KEY ("autorEmail") REFERENCES "User"("email") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Noticia" ADD CONSTRAINT "Noticia_tagSlug_fkey" FOREIGN KEY ("tagSlug") REFERENCES "Tag"("slug") ON DELETE RESTRICT ON UPDATE CASCADE;
